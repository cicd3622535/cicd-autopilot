variable "name" {
  description = "The name of our helm deployment (required)"
  default     = ""
  type        = string
}


variable "namespace" {
  description = "The name of our helm deployment (required)"
  default     = ""
  type        = string
}