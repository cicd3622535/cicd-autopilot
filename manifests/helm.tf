#resource "helm_release" "nginx" {
#  name = "nginx-new-deployment"

#  repository = "https://charts.bitnami.com/bitnami"
#  chart      = "nginx"

#  set {
#    name  = "service.type"
#    value = "ClusterIP"
#  }
#  provisioner "local-exec" {
#    command = "./helm.sh"
#    interpreter = ["shell", "-e"]
#  }
#}


resource "helm_release" "local-helm" {
  name = var.name
  namespace = var.namespace
 # repository = "https://charts.bitnami.com/bitnami"
  chart      = "../helm_charts/phoenixnap"
  provisioner "local-exec" {
    command = <<-EOT
           export name=$var.name
           chmod +x helm.sh
           ./helm.sh
           EOT
  }
  


}

