/* GLOBAL */
project_id = "clean-abacus-393308"
region     = "asia-south1"

workload_identity = {
  gcp_sa_name         = "abhi-sa"
  namespace           = "default"
  project_id          = "clean-abacus-393308"
  use_existing_k8s_sa = true
  k8s_sa_name         = "default"
  use_existing_gcp_sa = true
  k8s_sa_project_id   = "clean-abacus-393308"
}
