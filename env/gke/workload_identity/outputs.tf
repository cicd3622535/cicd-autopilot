/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

output "ksa_namespace" {
  description = "K8S service account namespace."
  value       = module.workload_identity.k8s_service_account_namespace
}

output "ksa_name" {
  description = "K8S SA name"
  value       = module.workload_identity.k8s_service_account_name
}

output "gsa_email" {
  description = "GCP service account."
  value       = module.workload_identity.gcp_service_account_email
}

output "gsa_name" {
  description = "K8S SA name"
  value       = module.workload_identity.gcp_service_account_name
}
