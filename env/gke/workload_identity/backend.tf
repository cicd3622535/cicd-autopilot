terraform {
  backend "gcs" {
    bucket = "ayushi-demo-tf-bucket"
    prefix = "env/dev/regions/asia-south1/gke/workload_identity"
  }
}