project_id = "clean-abacus-393308"
region     = "asia-south1"

/* GKE Control Plane */
cluster_name              = "cicd-cluster" 
regional                  = false
zones                     = ["asia-south1-a"]
network                   = "default"
subnetwork                = "default"
ip_range_pods             = "pod-range" 
ip_range_services         = "svc-range" 
service_account           = "abhi-sa@clean-abacus-393308.iam.gserviceaccount.com"
enable_private_endpoint   = true
enable_private_nodes      = true
release_channel           = "REGULAR"
kubernetes_version        = "1.27.3-gke.100"
master_ipv4_cidr_block    = "172.26.0.0/28"
default_max_pods_per_node = 110
master_authorized_networks = [
  {
    cidr_block   = "10.160.0.48/32"
    display_name = "VPC"
  }
]

remove_default_node_pool = true
cluster_resource_labels  = { owner = "ayushi", env = "dev" }
node_metadata            = "GKE_METADATA"
# identity_namespace       = null
