/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

data "google_compute_subnetwork" "subnetwork" {
  name    = var.subnetwork
  project = var.project_id
  region  = var.region
}

module "gke" {
  source     = "../../../modules/gke/private-cluster"
  project_id = var.project_id
  name       = var.cluster_name
  regional   = var.regional
  region     = var.region
  zones      = var.zones

  /* Networking */
  network                     = var.network
  subnetwork                  = var.subnetwork
  ip_range_pods               = var.ip_range_pods
  ip_range_services           = var.ip_range_services
  cluster_resource_labels     = var.cluster_resource_labels
  dns_cache                   = var.dns_cache
  http_load_balancing         = var.http_load_balancing
  network_policy              = var.network_policy
  network_policy_provider     = var.network_policy_provider
  enable_l4_ilb_subsetting    = var.enable_l4_ilb_subsetting
  enable_intranode_visibility = var.enable_intranode_visibility
  enable_private_endpoint     = var.enable_private_endpoint
  enable_private_nodes        = var.enable_private_nodes
  master_ipv4_cidr_block      = var.master_ipv4_cidr_block
  master_authorized_networks  = var.master_authorized_networks
  default_max_pods_per_node   = var.default_max_pods_per_node

  service_account    = var.service_account
  release_channel    = var.release_channel
  kubernetes_version = var.kubernetes_version

  /* Security*/
  enable_confidential_nodes    = var.enable_confidential_nodes
  enable_shielded_nodes        = var.enable_shielded_nodes
  enable_binary_authorization  = var.enable_binary_authorization
  node_metadata                = var.node_metadata
  identity_namespace           = var.identity_namespace
  authenticator_security_group = var.authenticator_security_group

  /* Feature */
  cloudrun                             = var.cloudrun
  logging_enabled_components           = var.logging_enabled_components
  monitoring_enabled_components        = var.monitoring_enabled_components
  monitoring_enable_managed_prometheus = var.monitoring_enable_managed_prometheus

  gce_pd_csi_driver       = var.gce_pd_csi_driver
  gke_backup_agent_config = var.gke_backup_agent_config
  enable_tpu              = var.enable_tpu
  enable_cost_allocation  = var.enable_cost_allocation
  filestore_csi_driver    = var.filestore_csi_driver

  maintenance_start_time          = var.maintenance_start_time
  maintenance_exclusions          = var.maintenance_exclusions
  maintenance_end_time            = var.maintenance_end_time
  maintenance_recurrence          = var.maintenance_recurrence
  notification_config_topic       = var.notification_config_topic
  enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling
  cluster_autoscaling             = var.cluster_autoscaling
  remove_default_node_pool        = var.remove_default_node_pool
  node_pools_tags                 = var.node_pools_tags  

  add_master_webhook_firewall_rules = var.add_master_webhook_firewall_rules
}
