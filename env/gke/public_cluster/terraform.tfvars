project_id = "searce-playground-v1"
region     = "asia-south1"

/* GKE Control Plane */
cluster_name                    = "pranav-public-cluster"
regional                        = false
zones                           = ["asia-south1-a"]
network                         = "saurabh-team-vpc"
subnetwork                      = "pranav-subnet"
ip_range_pods                   = "pod-range"
ip_range_services               = "service-range"
service_account                 = "ay-cicd@searce-playground-v1.iam.gserviceaccount.com"
release_channel                 = "REGULAR"
kubernetes_version              = "1.24.9-gke.3200"
default_max_pods_per_node       = 110
gcp_public_cidrs_access_enabled = false
master_authorized_networks = [
  {
    cidr_block   = "114.143.27.74/32"
    display_name = "VPC"
  }
]


remove_default_node_pool = true
cluster_resource_labels  = { owner = "pranav", env = "dev" }
node_metadata            = "GCE_METADATA"
identity_namespace       = null