project_id = "clean-abacus-393308"
region     = "asia-south1"

/* GKE Node Pool */

gke_node_pool = {
  cluster_name = "cicd-cluster"
  regional     = false
  region       = "asia-south1"
  zones        = ["asia-south1-a"]
  node_pools = [
    {
      name                        = "ayushi-pool-01"
      machine_type                = "e2-medium"
      node_locations              = "asia-south1-a"
      image_type                  = "COS_CONTAINERD"
      autoscaling                 = false
      min_count                   = 1
      max_count                   = 5
      disk_size_gb                = 50
      disk_type                   = "pd-standard"
      auto_upgrade                = true
      auto_repair                 = true
      spot                        = true
      initial_node_count          = 1
      service_account             = "28734378954-compute@developer.gserviceaccount.com"
      pod_range                   = "pod-range"
      enable_private_nodes        = true
      max_pods_per_node           = 20
      max_surge                   = 3
      max_unavailable             = 0
      enable_secure_boot          = false
      enable_integrity_monitoring = true
      node_metadata               = "GKE_METADATA"
    },
    {
      name                        = "ayushi-pool-02"
      machine_type                = "e2-medium"
      node_locations              = "asia-south1-b"
      image_type                  = "COS_CONTAINERD"
      autoscaling                 = false
      min_count                   = 1
      max_count                   = 5
      disk_size_gb                = 50
      disk_type                   = "pd-standard"
      auto_upgrade                = true
      auto_repair                 = true
      spot                        = true
      initial_node_count          = 1
      service_account             = "28734378954-compute@developer.gserviceaccount.com"
      pod_range                   = "pod-range"
      enable_private_nodes        = true
      max_pods_per_node           = 20
      max_surge                   = 3
      max_unavailable             = 0
      enable_secure_boot          = false
      enable_integrity_monitoring = true
      node_metadata               = "GKE_METADATA"
    }
  ]
  node_pools_labels = {
    env    = "dev"
  }
  node_pools_tags = []
}

node_pools_metadata     = {}
node_pools_taints       = {}
node_pools_oauth_scopes = {}

