/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

output "project_id" {
  value = var.project_id
}

output "cluster_name" {
  description = "Cluster name"
  value       = module.gke_node_pool.name
}

output "location" {
  description = "Cluster name"
  value       = module.gke_node_pool.location
}

output "node_pools_names" {
  description = "List of node pools names"
  value       = module.gke_node_pool.node_pools_names
}

output "node_pools_versions" {
  description = "Node pool versions by node pool name"
  value       = module.gke_node_pool.node_pools_versions
}

output "service_account" {
  description = "The service account to default running nodes as if not overridden in `node_pools`."
  value       = module.gke_node_pool.service_account
}

output "instance_group_urls" {
  description = "List of GKE generated instance groups"
  value       = module.gke_node_pool.instance_group_urls
}