/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/******************************************
  Get available zones in region
 *****************************************/
data "google_compute_zones" "available" {
  provider = google-beta

  project = var.project_id
  region  = local.region
}

resource "random_shuffle" "available_zones" {
  input        = data.google_compute_zones.available.names
  result_count = 3
}

locals {
  // ID of the cluster
  # cluster_id = google_container_cluster.primary.id
  cluster_network_tag = "gke-${var.cluster_name}"
  // location
  location = var.regional ? var.region : var.zones[0]
  region   = var.regional ? var.region : join("-", slice(split("-", var.zones[0]), 0, 2))
  // for regional cluster - use var.zones if provided, use available otherwise, for zonal cluster use var.zones with first element extracted
  node_locations = var.regional ? coalescelist(compact(var.zones), sort(random_shuffle.available_zones.result)) : slice(var.zones, 1, length(var.zones))
  // Build a map of maps of node pools from a list of objects
  node_pool_names = [for np in toset(var.node_pools) : np.name]
  node_pools      = zipmap(local.node_pool_names, tolist(toset(var.node_pools)))

  # network_project_id          = var.network_project_id != "" ? var.network_project_id : var.project_id
  zone_count = length(var.zones)
  // auto upgrade by defaults only for regional cluster as long it has multiple masters versus zonal clusters have only have a single master so upgrades are more dangerous.
  // When a release channel is used, node auto-upgrade are enabled and cannot be disabled.
  default_auto_upgrade = var.regional ? true : false

  // legacy mappings https://github.com/hashicorp/terraform-provider-google/pull/10238
  /* old_node_metadata_config_mapping = { GKE_METADATA_SERVER = "GKE_METADATA", GCE_METADATA = "EXPOSE" } */

  cluster_node_metadata_config = var.node_metadata == "UNSPECIFIED" ? [] : [{
    /* mode = lookup(local.old_node_metadata_config_mapping, var.node_metadata, var.node_metadata) */
    mode = var.node_metadata
  }]

  # /BETA features

  cluster_output_node_pools_names = concat(
    [for np in google_container_node_pool.pools : np.name], [""],
    # [for np in google_container_node_pool.windows_pools : np.name], [""]
  )

  cluster_output_node_pools_versions = merge(
    { for np in google_container_node_pool.pools : np.name => np.version },
    # { for np in google_container_node_pool.windows_pools : np.name => np.version },
  )

  // node pool ID is in the form projects/<project-id>/locations/<location>/clusters/<cluster-name>/nodePools/<nodepool-name>
  cluster_name_parts_from_nodepool = split("/", element(values(google_container_node_pool.pools)[*].id, 0))
  cluster_name_computed            = element(local.cluster_name_parts_from_nodepool, length(local.cluster_name_parts_from_nodepool) - 3)
  cluster_node_pools_names         = local.cluster_output_node_pools_names
  cluster_node_pools_versions      = local.cluster_output_node_pools_versions

}

/******************************************
  Get available container engine versions
 *****************************************/
data "google_container_engine_versions" "region" {
  location = local.location
  project  = var.project_id
}

data "google_container_engine_versions" "zone" {
  // Work around to prevent a lack of zone declaration from causing regional cluster creation from erroring out due to error
  //
  //     data.google_container_engine_versions.zone: Cannot determine zone: set in this resource, or set provider-level zone.
  //
  location = local.zone_count == 0 ? data.google_compute_zones.available.names[0] : var.zones[0]
  project  = var.project_id
}
