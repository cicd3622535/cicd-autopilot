/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file was automatically generated from a template in ./autogen/main

# output "cluster_name" {
#   description = "Cluster ID"
#   value       = google_container_node_pool.pools.cluster
# }

output "name" {
  description = "Cluster name"
  value       = local.cluster_name_computed
  depends_on = [
    /* Nominally, the cluster name is populated as soon as it is known to Terraform.
    * However, the cluster may not be in a usable state yet.  Therefore any
    * resources dependent on the cluster being up will fail to deploy.  With
    * this explicit dependency, dependent resources can wait for the cluster
    * to be up.
    */
    # google_container_cluster.primary,
    google_container_node_pool.pools,
  ]
} 

output "location" {
  description = "Cluster location (region if regional cluster, zone if zonal cluster)"
#   value       = google_container_node_pool.pools.location
  value       = [ for location in google_container_node_pool.pools : location.node_locations ]
}

output "node_pools_names" {
  description = "List of node pools names"
#   value       = google_container_node_pool.pools.cluster_node_pools_names
  value       = [for node_pools_names in google_container_node_pool.pools : node_pools_names.name ] 
}

output "node_pools_versions" {
  description = "Node pool versions by node pool name"
#   value       = google_container_node_pool.pools.version
  value       = [ for node_pools_versions in google_container_node_pool.pools : node_pools_versions.version ] 
}

output "service_account" {
  description = "The service account to default running nodes as if not overridden in `node_pools`."
#   value       = google_container_node_pool.pools.service_account
  value       = [ for service_account in google_container_node_pool.pools : service_account.node_config[*].service_account ] 
}

output "instance_group_urls" {
  description = "List of GKE generated instance groups"
  value       = distinct(flatten([for np in google_container_node_pool.pools : np.managed_instance_group_urls]))
}

